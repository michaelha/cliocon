outstanding todo:
- determine URL for retrieving post via REST API
- create mocks for testing
- write test cases and scenarios
- set up CI test (behat?) to check that the URL is returning 200
- work with ux designer on the design