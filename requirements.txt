Web Developer - MKTG
Coding Exercise Draft
Summary
This is a quick coding challenge, not a test. We want to get an understanding of how you approach a problem and how you solve one. We have outlined a task below that should allow you to showcase your skills. Good luck!
Task
In this project, you are required to set up a single page WordPress site where you can display a listing of a custom posts with custom fields using WordPress REST API. We would like these posts to be displayed on the page in a way that visually pleasing or unique, while also allowing the user to easily browse the listings for the post of his/her choosing. 

The custom post type should at least have these fields:
- Title
- Image
- Some meta data
- Description

To give you an idea of what we would like you to achieve, please take a look at Clio Cloud Conference speakers page: http://cliocloudconference.com/speakers/

Being a simple web project, you should use a mix of HTML, CSS, and JavaScript to build out your site. You are free to use 3rd party libraries or frameworks of your choosing, but be prepared to speak to your choices. Approach this problem as though it were an MVP for your future marketing site. Think about code organization and testing strategies to ensure quality for your customers and sanity for yourself!
Time
2-3 Days
Evaluation
Completed project should be delivered in a .zip file and any instructions we need to know to run the application needs be added as a readme file. 

The returned project will be evaluated on:
Functionality
Code organization 
Problem solving approach
Testing and test strategy

