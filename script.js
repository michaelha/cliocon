var app = angular.module("myApp", []);
app.controller("myCtrl", function ($scope, $http) {
    var profileUrl = "record.json"; // update profile URL with WordPress REST API endpoint
    $http.get(profileUrl)
        .then(function (response) {
            $scope.records = response.data;
        });
});